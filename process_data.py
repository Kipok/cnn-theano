import os
from scipy import misc
import gzip
import cPickle
import numpy


def change_mnist(mnistPath="D:/Documents/Projects/cnn/data/mnist.pkl.gz", newPath="D:/Documents/Projects/cnn/data/"):
    f_old = gzip.open(mnistPath, "rb")
    train_set, valid_set, test_set = cPickle.load(f_old)
    f_old.close()
    new_train_set = numpy.ndarray((train_set[0].shape[0], 3, train_set[0].shape[1]))
    new_valid_set = numpy.ndarray((valid_set[0].shape[0], 3, valid_set[0].shape[1]))
    new_test_set = numpy.ndarray((test_set[0].shape[0], 3, test_set[0].shape[1]))
    for i in xrange(train_set[0].shape[0]):
        new_train_set[i, 0, :] = new_train_set[i, 1, :] = new_train_set[i, 2, :] = train_set[0][i]
    for i in xrange(valid_set[0].shape[0]):
        new_valid_set[i, 0, :] = new_valid_set[i, 1, :] = new_valid_set[i, 2, :] = valid_set[0][i]
    for i in xrange(test_set[0].shape[0]):
        new_test_set[i, 0, :] = new_test_set[i, 1, :] = new_test_set[i, 2, :] = test_set[0][i]
    #f_new = open(newPath, "wb")
    #cPickle.dump(((new_train_set, train_set[1]), (new_valid_set, valid_set[1]), (new_test_set, test_set[1])), f_new, 2)
    f = open(newPath + 'mnist-valid.pkl', "wb")
    cPickle.dump((new_valid_set, valid_set[1]), f, 2)
    f.close()
    f = open(newPath + 'mnist-test.pkl', "wb")
    cPickle.dump((new_test_set, test_set[1]), f, 2)
    f.close()
    f = open(newPath + 'mnist-train-1.pkl', "wb")
    cPickle.dump((new_train_set[:12500], train_set[1][:12500]), f, 2)
    f.close()
    f = open(newPath + 'mnist-train-2.pkl', "wb")
    cPickle.dump((new_train_set[12500:25000], train_set[1][12500:25000]), f, 2)
    f.close()
    f = open(newPath + 'mnist-train-3.pkl', "wb")
    cPickle.dump((new_train_set[25000:37500], train_set[1][25000:37500]), f, 2)
    f.close()
    f = open(newPath + 'mnist-train-4.pkl', "wb")
    cPickle.dump((new_train_set[37500:50000], train_set[1][37500:50000]), f, 2)
    f.close()


def shuffle_in_unison(a, b):
    assert len(a) == len(b)
    shuffled_a = numpy.empty(a.shape, dtype=a.dtype)
    shuffled_b = numpy.empty(b.shape, dtype=b.dtype)
    permutation = numpy.random.permutation(len(a))
    for old_index, new_index in enumerate(permutation):
        shuffled_a[new_index] = a[old_index]
        shuffled_b[new_index] = b[old_index]
    return shuffled_a, shuffled_b


def process_images(dirname='D:/Documents/Projects/cnn/data/JPEGImages'):
    images = numpy.ndarray(shape=(0, 3, 128 * 128), dtype=numpy.uint8)
    labels = numpy.ndarray(shape=(0,), dtype=numpy.uint8)
    i = 0
    print 'reading images ...'
    for filename in os.listdir(dirname):
        i += 1
        img = misc.imread(os.path.join(dirname, filename))
        img = misc.imresize(img, (128, 128))
        if img.shape != (128, 128, 3):
            continue
        img = numpy.swapaxes(img, 1, 2)
        img = numpy.swapaxes(img, 0, 1)
        img = numpy.reshape(img, (3, 128 * 128))
        images = numpy.append(images, numpy.reshape(img, (1, img.shape[0], img.shape[1])), axis=0)
        if len(filename) <= 9:
            labels = numpy.append(labels, [1])
        else:
            labels = numpy.append(labels, [0])
       # print 'File: {}, Label: {}'.format(filename, labels[-1])
    print 'writing to file ...'
    images, labels = shuffle_in_unison(images, labels)
    train_size = 20000
    valid_size = 4452
    test_size = 4452
    train_set = (images[:train_size], labels[:train_size])
    valid_set = (images[train_size:train_size + valid_size], labels[train_size:train_size + valid_size])
    test_set = (images[train_size + valid_size:train_size + valid_size + test_size], labels[train_size + valid_size:train_size + valid_size + test_size])
    f = open('D:/Documents/Projects/cnn/data/birds-valid.pkl', "wb")
    cPickle.dump(valid_set, f, 2)
    f.close()
    f = open('D:/Documents/Projects/cnn/data/birds-test.pkl', "wb")
    cPickle.dump(test_set, f, 2)
    f.close()
    f = open('D:/Documents/Projects/cnn/data/birds-train-1.pkl', "wb")
    cPickle.dump((train_set[0][:5000], train_set[1][:5000]), f, 2)
    f.close()
    f = open('D:/Documents/Projects/cnn/data/birds-train-2.pkl', "wb")
    cPickle.dump((train_set[0][5000:10000], train_set[1][5000:10000]), f, 2)
    f.close()
    f = open('D:/Documents/Projects/cnn/data/birds-train-3.pkl', "wb")
    cPickle.dump((train_set[0][10000:15000], train_set[1][10000:15000]), f, 2)
    f.close()
    f = open('D:/Documents/Projects/cnn/data/birds-train-4.pkl', "wb")
    cPickle.dump((train_set[0][15000:20000], train_set[1][15000:20000]), f, 2)
    f.close()


def check_images(filename='D:/Documents/Projects/cnn/data/'):
    file = open(filename, "rb")
    train_set = cPickle.load(file)
    img = numpy.reshape(train_set[0][0], (3, 128, 128))
    misc.imsave('D:/Documents/Projects/cnn/data/1.png', img[0])


def post_process(dirname='D:/Documents/Projects/cnn/data/img'):
    for filename in os.listdir(dirname):
        file = open(os.path.join(dirname, filename), "rb")
        data_set = cPickle.load(file)
        new_data_x = data_set[0].astype(numpy.float32) / 255
        f = open(os.path.join(dirname, filename[:-4] + 'n.pkl'), "wb")
        cPickle.dump((new_data_x, data_set[1]), f, 2)
        file.close()
        f.close()


if __name__ == '__main__':
    post_process()
